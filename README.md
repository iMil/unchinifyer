# ieGeek camera unchinifyer

## WARNING

## I AM IN NO WAY RESPONSIBLE FOR ANY HARM THIS COULD CAUSE TO ANY OF YOUR DEVICES

## What is this?

This project explains how to take control over an ieGeek security webcam and
make it stop leaking data to chinese servers.

![ieGeek 720p security webcam](images/iegeek-cam.png)

## What do you need?

* Any micro SD card will do.
* A _slightly modified_ [`squashfs rootfs` image][1]

## Recipe

* Format the micro SD card using FAT32, for example

```sh
# mkfs.vfat /dev/sdf1
```

* Assuming the SD card is mounted in `/mnt`, create the following directory structure:

```sh
# mkdir /mnt/gwell_npc_upg_pack/input
```

* Copy the modified `rootfs` in this directory (the image **must** have this name):

```sh
# cp rootfs_gm_small.bin /mnt/gwell_npc_upg_pack/input/
```

* Create a `/mnt/ext.sh` file with whatever you want to be executed at boot, i.e.:

```sh
# cat /mnt/ext.sh
/usr/sbin/telnetd
# make "twitch" the root password
sed 's,root::,root:$1$1woAE2iT$O5Fnug.C5ESnZBOC3SWQs/:,' /etc/shadow >/tmp/s
mv /tmp/s /etc/shadow
```

* Boot the camera and press the _reset_ button for about 10 seconds, the camera will flash its root filesystem with the new one and when it's done (~30 seconds) it will reboot and execute `ext.sh`

At this point the webcam is a useless device running a stripped down Linux-based operating system, but fortunately you can use [this project][2] and download a working [rtsp server][3] that just has to be added to `ext.sh`.

## Standard boot

If you still want the camera to run its original program while still having `telnet` access to it, simply call `/patch/bin/startup` from the `ext.sh` script, but beware all automatic cam recording and connections to `.cn` servers will resume.

## Longer story

This "security camera" records videos every 30 minutes, whether you like it or not, and calls home every minute, a simple `tcpdump` will make you want unplug it from your network.

The device happens to have an [UART][4] near the _Grain Media 8135 CPU_:

![ieGeek camera opened](images/opened.jpg)

So we can attach a [screen][7] console to it, using for example an _USB_ serial:

```sh
$ screen /dev/ttyUSB0 115200
```

And as one might expect, it runs a Linux kernel with a stripped down userland revolving around [busybox][5], well, actually a _GM_ (_Grain Media_) minimal version of it. If you want to build your own userland using _GM_'s _SDK_, [here are some pointers][6]. 

The console is very verbose, lots of logs coming from camera kernel modules, but also what appears to be the only process running: `/npc/npc`.  
This program does not appear to be part of _GM_ and seems to handle **everything**, from capture to recording, including transmissions to motherland China.

Using the console is pretty painful as it constantly outputs log, fortunately they left the `telnetd` applet in `busybox`, so it can be launched and used, no password is needed.  
Using a micro SD card, we can bring some convenience to the device, first a more complete `busybox`, it needs to be built for _ARM 32 bits_, I used the one from [the official `busybox` download site](https://busybox.net/downloads/binaries/1.21.1/busybox-armv7l).

If you try to kill the `npc` process, the camera will reboot within seconds, if you try to block its outside traffic it will also reboot and if you delete its default route... you guessed it, it will reboot. This camera is **unusable** for the mortal if it's not connected to the Internet.

Next up I analyzed this `npc` program using [Ghidra][8] only to find loads of Chinese servers addresses being hardcoded within the program, but more importantly, a test for a file called `HostList.txt`, which happened to contain many addresses.  
In order to make the camera stop calling home, I modified this `/rom/HostList.txt` file with the following:

```txt
HostListNameStr:|192.168.1.31
```
`192.168.1.31` being the camera's IP. Strangely enough, `127.0.0.1` is not recognized as a valid IP address and a resolving request is sent to my _DNS_ server.

Once done, the camera is much more silent, but some _DNS_ queries still get through, so I started `busybox`'s mini-DNS server and replaced the resolver in `/etc/resolv.conf`:

```sh
[root@GM]# /mnt/disc1/bin/busybox dnsd -d
[root@GM]# echo "nameserver 127.0.0.1" >/etc/resolv.conf
```

Now the camera is pretty silent, only a couple of _ARP_ requests keep coming, which is completely normal as `udhcpc` is running.  
Nevertheless, the `/mnt/disc1/npc/record` is still filling with video recordings triggered every 30 minutes (WTF???), so I definitely needed to hunt down `npc`.

Analyzing the boot process, we can see that the script called by the minimal `init` system is `/boot.sh`:

```sh
[root@GM]# busybox tail -8 /etc/init.d/rc.sysinit 
/gm/bin/mke2fs /dev/ram0
mount  /dev/ram0 /mnt/ramdisk
cp -R  /etc/* /mnt/ramdisk
mount  /mnt/ramdisk/  /etc
mount  /mnt/ramdisk/  /tmp

mount -t jffs2 /dev/mtdblock4 /npc
sh boot.sh
```
`boot.sh` in turn calls `/patch/bin/startup`, which is responsible for the initial setup of this webcam.  
`/patch/bin/startup` is a quite interesting piece of program because very conveniently, the vendor forgot to strip its debugging symbols, thus making it a very readable disassembled code. More specifically, we can see that the function `fgStartCheckUpdatePack` checks for an existing directory in the SD card, and after that if a particular file exists in that directory:

![gwell_npc_upg_pack exists](images/gwell_npc_upg_pack.png)

![rootfs exists](images/rootfs_gm_small_bin.png)

As a matter of fact, `rootfs_gm_small_bin` is a `squashfs` disc image, and actually what's in `/dev/mtd2`!  
In order to be able to flash back if anything goes wrong, let's first dump this partition:

On a different machine:
```sh
$ nc -l 192.168.1.2 -p 2121 >mtd2
```

On the camera:
```sh
[root@GM]# /mnt/disc1/bin/busybox nc 192.168.1.2 2121 < /dev/mtd2
```

Now that we have a dump from root filesystem, let's copy it to the destination file:

```sh
$ cp mtd2 rootfs_gm_small_bin
```
Let's check what it is made of:
```sh
$ file rootfs_gm_small_bin
rootfs_gm_small_bin: Squashfs filesystem, little endian, version 1024.0, compressed, -2670300327495860224 bytes, -1728053248 inodes, blocksize: 512 bytes, created: Thu Mar 22 06:52:54 2068
```
Let's try and mount it:
```sh
$ sudo mount -o loop rootfs_gm_small_bin /mnt/rootfs
```
And here we go:
```sh
bin      dev  gm    init  mnt  opt    proc  root  share          sys  usr
boot.sh  etc  home  lib   npc  patch  rom   sbin  squashfs_init  tmp  var
```
Let's sync this to a writable directory:

```sh
$ sudo rsync -av /mnt/rootfs newmtd2
```
And slightly modify the `/boot.sh` file:
```sh
#echo =========================================================================
#echo " Start startup!"
#/patch/bin/startup
echo =========================================================================
echo "FUNZORZ BITCH"
echo =========================================================================
/bin/mount -t vfat /dev/mmcblk0p1 /mnt/disc1
/bin/sh /mnt/disc1/ext.sh
```
Let's create our alternative `rootfs` using `mksquashfs`
```sh
# mksquashfs newmtd2 rootfs_gm_small.bin -comp xz
```
And copy it where `startup` expects it:
```sh
$ sudo cp rootfs_gm_small.bin /mnt/sdcard/gwell_npc_upg_pack/input/
```
The flashing does not occur automatically after booting up the camera with the SD card populated with our payload, as we can see here:

![press reset](images/press_reset.png)

When pressing reset while the camera boots, it calls the flashing procedure, which involves `/gm/bin/flash_eraseall` and `/gm/tools/flashcp`, called from the `fgUpdateRootfs` function.  
If all goes well and you have a serial console plugged, you should see the `mtd2` partition being erased with a progression percentage, and then a very frightening list of `0` and `1` during the flashing process.

When the flashing process is finished, the camera will reboot and will **not** start `/patch/bin/startup` which in turn will **not** start `/npc/npc`, and suddenly the camera will be as silent as useless.

Fortunately, _GM_ has generic tools for their camera, and among them a perfectly functional _RTSP_ server which you can grab following [this project](https://github.com/ghoost82/mijia-720p-hack) or by downloading [GM's SDK][6].

Here's the content of my `ext.sh` file:

```sh
/usr/sbin/telnetd
# generate password hash using openssl passwd -1
sed 's,root::,root:$1$somehash/:,' /etc/shadow >/tmp/s
mv /tmp/s /etc/shadow

SDCARD=/mnt/disc1

cd $SDCARD

# if we want to toy with npc and startup
if [ -f runnpc ]; then
        /patch/bin/startup
else
	# load kernel modules needed by this camera
        /npc/mtd/vg_boot.sh sc1045 PAL
        sleep 5
        cp ${SDCARD}/etc/own/wpa_supplicant0.conf /rom/
        /patch/bin/wpa_supplicant -i wlan0 -B -c /rom/wpa_supplicant0.conf
        sleep 5
        /sbin/udhcpc -i wlan0 -b -s /npc/dhcp.script
fi

cp etc/own/profile /etc/
bin/busybox dnsd -d
echo "nameserver 127.0.0.1" >/etc/resolv.conf

for f in vi touch which nohup
do
        ln -s ${SDCARD}/bin/busybox /bin/${f}
done

nohup bin/rtspd -s &
```

**Happy Hacking!**

## Greets

* https://herrfeder.github.io/embeddedsec/2017/10/19/Hacking-A-IP-Camera-Part1.html
* https://github.com/ghoost82/mijia-720p-hack

[1]: https://mega.nz/file/B41HSYpC#_M6rtr2Nnio67CMPDKCrdN4XXC7U4roKqtxvV3MdXME
[2]: https://github.com/ghoost82/mijia-720p-hack
[3]: https://github.com/ghoost82/mijia-720p-hack/releases/download/v0.95/mijia-720p-hack.zip
[4]: https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter
[5]: https://busybox.net/
[6]: https://github.com/Filipowicz251/mijia-1080P-hacks/wiki
[7]: https://www.gnu.org/software/screen/
[8]: https://ghidra-sre.org/
